import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import BrowserPersistence from '@util/simplePersistence';

const defaultOptions = {
	watchQuery: {
		fetchPolicy: 'cache-and-network',
		errorPolicy: 'ignore'
	},
	query: {
		fetchPolicy: 'network-only',
		errorPolicy: 'all'
	},
	mutate: {
		errorPolicy: 'all'
	}
};

const apiBase = new URL(BACKEND_URL).toString();

const httpLink = createHttpLink({
	uri: apiBase + 'graphql'
});

const authLink = setContext((_, { headers }) => {
	const storage = new BrowserPersistence();
	const token = storage.getItem('token');

	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : ''
		}
	};
});

export const client = new ApolloClient({
	uri: apiBase,
	cache: new InMemoryCache(),
	link: authLink.concat(httpLink),
	defaultOptions
});
