import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from '@components/App';

import store from './store';
import ThemeContextProvider from './context';
import { ApolloProvider } from '@apollo/client';
import { IntlProvider } from 'react-intl';
import './ant.less';

import translateMessages from '@util/getLocationMessage';
import { client } from './configs/apollo';

ReactDOM.render(
	<React.Fragment>
		<Provider store={store}>
			<ThemeContextProvider>
				<IntlProvider
					locale="en"
					defaultLocale="en"
					messages={translateMessages}
				>
					<ApolloProvider client={client}>
						<App />
					</ApolloProvider>
				</IntlProvider>
			</ThemeContextProvider>
		</Provider>
	</React.Fragment>,
	document.getElementById('root')
);
