import React from 'react';
import Customer from './Customer';
import guest from './Guest';

export const customerLayout = Customer;
export const guestLayout = guest;
