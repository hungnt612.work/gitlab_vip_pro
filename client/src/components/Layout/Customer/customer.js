import React, { useEffect } from 'react';
import classes from './customer.scss';

import { useHistory } from 'react-router-dom';

import Header from './header';

import BrowserPersistence from '@util/simplePersistence';

const storage = new BrowserPersistence();

const token = storage.getItem('token');

import { PageHeader } from 'antd';

const CustomerLayout = (Component) => {
	const history = useHistory();

	useEffect(() => {
		if (!token) {
			// history.push('/user/signIn');
			// window.location = '/user/signIn';
		}
	}, [history]);

	return (
		<div className={classes.root}>
			<PageHeader
				className="site-page-header"
				// onBack={() => {
				// 	history.goBack();
				// }}
				title="Title"
				subTitle="This is a subtitle"
			/>
			<div className={classes.container}>
				<Component />
			</div>
		</div>
	);
};

export default CustomerLayout;
