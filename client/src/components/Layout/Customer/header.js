import React from 'react';
import { shape, string } from 'prop-types';

import logo from '@digicard/src/static/images/Logo.png';

import classes from './customer.scss';
import { NotificationTwoTone, UserOutlined } from '@ant-design/icons';
import LanguageSelector from '@components/LanguageSelector';

const Header = (props = {}) => {
	return (
		<header className={classes.header}>
			<div className={classes.info}>
				<img src={logo} className={classes.logo}  alt="Digicard" />

                <LanguageSelector shouldHideName={true} classes={{
                    root: classes.languagueSelector
                }}/>
			</div>
			<div className={classes.actions}>
				<div className={classes.notification}>
					<NotificationTwoTone />
				</div>
				<div className={classes.user}>
					<UserOutlined />
				</div>
			</div>
		</header>
	);
};

Header.propTypes = {
	classes: shape({ root: string })
};
Header.defaultProps = {};
export default Header;
