import React, { useCallback } from 'react';
import { shape, string } from 'prop-types';

import defaultClasses from './languageSelector.scss';
import mergeClasses from '@util/mergeClasses';
import { Select } from 'antd';

const { Option } = Select;

import englishFlag from '@digicard/src/static/images/united.png';
import vietNamFlag from '@digicard/src/static/images/vietnam.png';
import czenchFlag from '@digicard/src/static/images/czech.png';

import BrowserPersistence from '@util/simplePersistence';

const storage = new BrowserPersistence();

const languageOptions = [
	{
		label: 'English',
		image: englishFlag,
		value: 'en'
	},
	{
		label: 'Tiếng Việt',
		image: vietNamFlag,
		value: 'vi'
	},
	{
		label: 'Czench',
		image: czenchFlag,
		value: 'cz'
	}
];

const LanguageSelector = (props = {}) => {
	const classes = mergeClasses(defaultClasses, props.classes);
	const { shouldHideName } = props;
	const currentLanguage = storage.getItem('lang') || 'en';

	const options = languageOptions.map((option) => {
		return (
			<Option value={option.value}>
				<img src={option.image} /> {!shouldHideName && option.label}
			</Option>
		);
	});

	const handleChangeLanguage = useCallback((value) => {
		storage.setItem('lang', value);
		setTimeout(() => {
			window.location.reload();
		}, 300);
	}, []);

	return (
		<div className={classes.root}>
			<Select
				defaultValue={currentLanguage}
				onChange={handleChangeLanguage}
			>
				{options}
			</Select>
		</div>
	);
};

LanguageSelector.propTypes = {
	classes: shape({ root: string })
};

LanguageSelector.defaultProps = {};

export default LanguageSelector;
