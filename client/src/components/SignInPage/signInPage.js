import React from 'react';

import defaultClasses from './signInPage.scss';
import logo from '@digicard/src/static/images/Logo.png';
import { Form, Input, Button, Checkbox } from 'antd';

import mergeClasses from '@util/mergeClasses';
import { FormattedMessage, useIntl } from 'react-intl';
import { UserOutlined, KeyOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { useSignInPage } from '@digicard/src/talons/SignInPage/useSignInPage';
import LanguageSelector from '@components/LanguageSelector';

const SignInPage = (props) => {
    const classes = mergeClasses(defaultClasses, props.classes);

    const talonProps = useSignInPage();

    const { handleLogin, isLoading } = talonProps;

    const { formatMessage } = useIntl();

    return (
        <div className={classes.root}>
            <div className={classes.header}>
                <div className={classes.location}>
                    <LanguageSelector />
                </div>
            </div>
            <div className={classes.content}>
                <div className={classes.logo}>
                    <img src={logo} alt={'logo'} />
                </div>
                <div className={classes.form}>
                    <h3>
                        <FormattedMessage
                            defaultMessage={'Sign In'}
                            id={'signInPage.login'}
                        />
                    </h3>
                    <Form onFinish={handleLogin}>
                        <Form.Item
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: formatMessage({
                                        id: 'signInPage.requireUsername',
                                        defaultMessage: 'Please enter username'
                                    })
                                }
                            ]}
                            className={classes.formItem}
                        >
                            <Input
                                prefix={
                                    <UserOutlined className={classes.icon} />
                                }
                                placeholder={formatMessage({
                                    id: 'signInPage.username',
                                    defaultMessage: 'Username'
                                })}
                                className={classes.input}
                            />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: formatMessage({
                                        id: 'signInPage.requirePassword',
                                        defaultMessage: 'Please enter password'
                                    })
                                }
                            ]}
                            className={classes.formItem}
                        >
                            <Input
                                prefix={
                                    <KeyOutlined className={classes.icon} />
                                }
                                type="password"
                                placeholder={formatMessage({
                                    id: 'signInPage.password',
                                    defaultMessage: 'Password'
                                })}
                                className={classes.input}
                            />
                        </Form.Item>

                        <div className={classes.detail}>
                            <Checkbox name="remember">
                                {formatMessage({
                                    id: 'signInPage.remember',
                                    defaultMessage: 'Remember me'
                                })}
                            </Checkbox>

                            <Link to={'/password/reset'}>
                                {formatMessage({
                                    id: 'signInPage.forgotPassword',
                                    defaultMessage: 'Forgot password ? '
                                })}
                            </Link>
                        </div>

                        <Button
                            type="primary"
                            className={classes.btnSignIn}
                            htmlType="submit"
                            loading={isLoading}
                        >
                            <FormattedMessage
                                defaultMessage={'Sign In'}
                                id={'signInPage.login'}
                            />
                        </Button>
                    </Form>
                </div>
            </div>
            <div className={classes.footer}></div>
        </div>
    );
};
export default SignInPage;
