import React from 'react';
import Router from '@components/Router';
import { hot } from 'react-hot-loader/root';

const App = (props = {}) => {
    return (
        <div>
            <Router />
        </div>
    );
};

export default hot(App);
