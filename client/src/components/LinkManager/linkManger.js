import React, { useCallback } from 'react';
import { shape, string } from 'prop-types';

import classes from './linkManger.scss';
import { Tabs } from 'antd';
import { useIntl } from 'react-intl';
import Profile from '@components/LinkManager/Profile';
import Social from '@components/LinkManager/Social';
import ViewCard from '@components/ViewCard/viewCard';

import { useManager } from '@digicard/src/talons/LinkManager/useManager';
import { UserOutlined, FormatPainterOutlined, HomeOutlined, LinkOutlined, SettingOutlined } from '@ant-design/icons';

const { TabPane } = Tabs;

const LinkManger = (props) => {
	const talonProps = useManager();
	const { onCallbackLink, links } = talonProps;

	const { formatMessage } = useIntl();

    const homeTabIcon = (
        <div className={classes.tabIcon}>
            <HomeOutlined />
            <span>
				{formatMessage({
                    id: 'linkManger.details',
                    defaultMessage: 'Thông tin'
                })}
			</span>
        </div>
    );

	const infoTabIcon = (
		<div className={classes.tabIcon}>
			<UserOutlined />
			<span>
				{formatMessage({
					id: 'linkManger.details',
					defaultMessage: 'Thông tin'
				})}
			</span>
		</div>
	);

	const linkTabIcon = (
		<div className={classes.tabIcon}>
            <LinkOutlined />
			<span>
				{formatMessage({
					id: 'linkManger.link',
					defaultMessage: 'Links'
				})}
			</span>
		</div>
	);

	const configTabIcon = (
		<div className={classes.tabIcon}>
			<FormatPainterOutlined />
			<span>
				{formatMessage({
					id: 'linkManger.config',
					defaultMessage: 'Themes'
				})}
			</span>
		</div>
	);

    const settingTabIcon = (
        <div className={classes.tabIcon}>
            <SettingOutlined />
            <span>
				{formatMessage({
                    id: 'linkManger.setting',
                    defaultMessage: 'Setting'
                })}
			</span>
        </div>
    );

	return (
		<div className={classes.root}>
			<div>
				<Tabs defaultActiveKey="0" className={classes.settings}>
                    <TabPane tab={homeTabIcon} key="0">
                        <div className={classes.tabContent}>
                            <h1>Home Tabs</h1>
                        </div>
                    </TabPane>

					<TabPane tab={infoTabIcon} key="1">
						<div className={classes.tabContent}>
							<Profile />
						</div>
					</TabPane>
					<TabPane tab={linkTabIcon} key="2">
						<div className={classes.tabContent}>
							<Social onCallback={onCallbackLink} />
						</div>
					</TabPane>

                    <TabPane tab={configTabIcon} key="4">
                        <div className={classes.tabContent}>
                            config
                        </div>
                    </TabPane>

                    <TabPane tab={settingTabIcon} key="5">
                        <div className={classes.tabContent}>
                            setting
                        </div>
                    </TabPane>

				</Tabs>
			</div>

			<div className={classes.preview}>
				<ViewCard isPreview={true} links={links} />
			</div>
		</div>
	);
};

LinkManger.propTypes = {
	classes: shape({ root: string })
};
LinkManger.defaultProps = {};
export default LinkManger;
