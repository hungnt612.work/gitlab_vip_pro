import React, { useCallback } from 'react';
import { shape, string } from 'prop-types';
import {
	Form,
	Input,
	Button,
	Radio,
	Select,
	Cascader,
	DatePicker,
	InputNumber,
	TreeSelect,
	Switch
} from 'antd';
import classes from './profile.scss';
import { useIntl } from 'react-intl';

const Profile = (props) => {
	const user = {
		avatar: 'https://i.pinimg.com/564x/44/15/ba/4415ba5df0f4bfcee5893d6c441577e0.jpg'
	};

	const { formatMessage } = useIntl();

	return (
		<div className={classes.root}>
			<img src={user.avatar} className={classes.avatar} />

			<Form
				layout="horizontal"
				labelCol={{ span: 3 }}
				className={classes.form}
			>
				<h3>
					{formatMessage({
						id: 'profile.title',
						defaultMessage: 'Adjust Information'
					})}
				</h3>

				<Form.Item
					label={formatMessage({
						id: 'profile.fullname',
						defaultMessage: 'Full name'
					})}
				>
					<Input name={'fullName'} />
				</Form.Item>
				<Form.Item
					label={formatMessage({
						id: 'profile.bio',
						defaultMessage: 'Bio'
					})}
				>
					<Input.TextArea name="bio" rows={4} />
				</Form.Item>

				<Form.Item
					label={formatMessage({
						id: 'profile.email',
						defaultMessage: 'Email'
					})}
				>
					<Input name={'email'} />
				</Form.Item>

				<Form.Item
					label={formatMessage({
						id: 'profile.phone',
						defaultMessage: 'Phone'
					})}
				>
					<Input name={'phone'} />
				</Form.Item>

				<Form.Item
					label={formatMessage({
						id: 'profile.pin',
						defaultMessage: 'Pin'
					})}
					valuePropName="checked"
				>
					<Switch />
                    <Input name={'pin'} />
				</Form.Item>


				<div className={classes.actions}>
					<Button type={'primary'} className={classes.submit}>
						{formatMessage({
							id: 'profile.submit',
							defaultMessage: 'Save changes'
						})}
					</Button>
				</div>
			</Form>
		</div>
	);
};
export default Profile;
