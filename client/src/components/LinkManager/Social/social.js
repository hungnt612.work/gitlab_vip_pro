import React, { useMemo, useState } from 'react';

import classes from './social.scss';
import { useIntl } from 'react-intl';
import { Button, Modal, Select, Popover } from 'antd';

import { useSocial } from '@digicard/src/talons/LinkManager/useSocial';
import { Sortable } from '@progress/kendo-react-sortable';
import Item from '@components/LinkManager/Social/item';
import { HeartOutlined } from '@ant-design/icons';

import supportSocialTypes from '@digicard/src/talons/LinkManager/socialItems.json';

const getBaseItemStyle = (isActive) => ({
	// opacity: isActive ? 0.5 : 1
});

const SortableItemUI = (props) => {
	const {
		isActive,
		style,
		attributes,
		dataItem,
		forwardRef,
		onDeleteItem,
		onEditItem
	} = props;

	return (
		<div
			ref={forwardRef}
			{...attributes}
			style={{ ...getBaseItemStyle(isActive), ...style }}
		>
			<Item
				data={dataItem}
				onDeleteItem={onDeleteItem}
				onEditItem={onEditItem}
				isActive={isActive}
				{...attributes}
			/>
		</div>
	);
};

const Social = (props = {}) => {
	const { onCallback } = props;
	const { formatMessage } = useIntl();
	const [selectedSocialType, setSelectedSocialType] = useState();

	const talonProps = useSocial({ onCallback });
	const {
		toggleCreateModal,
		handleAddLink,
		items,
		onDeleteItem,
		onDragOver,
		onNavigate,
		onEditItem,
		isShowCreateModal
	} = talonProps;

	const socialType = useMemo(() => {
		const availableSocialTypes = supportSocialTypes.filter((option) => {
			// const wasAdded = items.some((item) => item.type === option.type);
			// return !wasAdded;
			return true;
		});

		const socialTypeOptions = availableSocialTypes.map((social) => {
			const { type, label, icon } = social;
			return (
				<Select.Option value={type}>
					<div className={classes.option}>
						<img src={icon} width={15} height={15} />
						{label}
					</div>
				</Select.Option>
			);
		});

		return (
			<div style={{ width: '400px' }}>
				<Select
					onChange={(value) => {
						handleAddLink(value);
					}}
					className={classes.select}
					placeholder={formatMessage({
						id: 'social.selectSocialType',
						defaultMessage: 'Please select social type'
					})}
				>
					{socialTypeOptions}
				</Select>
			</div>
		);
	}, [setSelectedSocialType, handleAddLink, items, isShowCreateModal]);

	return (
		<div className={classes.root}>
			<div className={classes.actions}>
				<Popover
					title={formatMessage({
						id: 'social.pleaseChooseSocial',
						defaultMessage: 'Please choose social type'
					})}
					placement="top"
					content={socialType}
					trigger="click"
					visible={isShowCreateModal}
				>
					<Button
						className={classes.newLink}
						type={'primary'}
						onClick={() => {
							toggleCreateModal(true);
						}}
					>
						{formatMessage({
							id: 'social.new',
							defaultMessage: 'Add links'
						})}
					</Button>
				</Popover>
				<Button
					className={classes.magic}
					type={'primary'}
					icon={<HeartOutlined />}
				/>
			</div>
			<div className={classes.container}>
				<Sortable
					idField={'type'}
					data={items}
					itemUI={(props) => (
						<SortableItemUI
							{...props}
							onDeleteItem={onDeleteItem}
							onEditItem={onEditItem}
						/>
					)}
					onDragOver={onDragOver}
					onNavigate={onNavigate}
				/>
			</div>
		</div>
	);
};
export default Social;
