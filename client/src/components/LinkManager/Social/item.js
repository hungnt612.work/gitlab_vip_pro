import React from 'react';

import classes from './item.scss';
import { useIntl } from 'react-intl';
import { Input, Button, Switch, Popconfirm, Form } from 'antd';
import { DeleteOutlined, MoreOutlined } from '@ant-design/icons';

const Item = (props = {}) => {
	const { data, onDeleteItem, onEditItem, isActive } = props;
	const { icon, label, input, placeholder, type, value, visible } =
		data || {};

	const { formatMessage } = useIntl();

	return (
		<Form
			initialValues={{
				label,
				value
			}}
		>
			<div className={`${classes.root} ${isActive && classes.active}`}>
				<div className={classes.moveIcon}>
					<MoreOutlined />
				</div>

				<div className={classes.icon}>
					<img src={icon} width={30} height={30} />
				</div>
				<div className={classes.content}>
					<Form.Item className={classes.title} name="label">
						<Input
							name="label"
							placeholder={label}
							allowClear
							defaultValue={label}
							className={classes.input}
							onBlur={(e) => {
								const value = e.target.value;
								onEditItem(type, {
									label: value
								});
							}}
						/>
					</Form.Item>
					<Form.Item className={classes.link} name="value">
						{input === 'textArea' ? (
							<Input.TextArea
								name="value"
								placeholder={placeholder}
								rows={2}
								className={classes.input}
								onBlur={(e) => {
									const value = e.target.value;
									onEditItem(type, {
										value
									});
								}}
							/>
						) : (
							<Input
								name="value"
								placeholder={placeholder}
								allowClear
								className={classes.input}
								onBlur={(e) => {
									const value = e.target.value;
									onEditItem(type, {
										value
									});
								}}
							/>
						)}
					</Form.Item>
				</div>

				<div className={classes.actions}>
					<Switch
						className={classes.switchIcon}
						name={'visible'}
						defaultChecked={visible}
						onChange={(checked) => {
							onEditItem(type, {
								visible: checked
							});
						}}
					/>
					<Popconfirm
						title={formatMessage({
							id: 'item.areYouWantDelete',
							defaultMessage: 'Are you want delete ? '
						})}
						onConfirm={() => onDeleteItem(type)}
						okText="Yes"
						cancelText="No"
					>
						<Button className={classes.delete}>
							<DeleteOutlined className={classes.deleteIcon} />
						</Button>
					</Popconfirm>
				</div>
			</div>
		</Form>
	);
};

export default Item;
