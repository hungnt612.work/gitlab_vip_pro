import React from 'react';
import mergeClasses from '@digicard/src/util/mergeClasses';
import { shape, string } from 'prop-types';

import AntButton from 'antd/lib/button';

import defaultClasses from './button.scss';

const Button = (props) => {
    const classes = mergeClasses(defaultClasses, props.classes);
    return <AntButton {...props} className={classes.button} />;
};

Button.propTypes = {
    classes: shape({ root: string })
};
Button.defaultProps = {};
export default Button;
