import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Layout
import { guestLayout, customerLayout } from '@components/Layout';

// Guest Component
import SignInPage from '@components/SignInPage';
import LinkManager from '@components/LinkManager';

/*
 * TODO: Write routers in routers.json
 */

const AppRouter = (props) => {
	return (
		<Router>
			<Switch>
				<Route path="/user/signIn" exact={true}>{guestLayout(SignInPage)}</Route>
				<Route path="/user" exact={false}>{customerLayout(LinkManager)}</Route>
			</Switch>
		</Router>
	);
};
export default AppRouter;
