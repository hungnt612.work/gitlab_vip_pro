import React, { useMemo } from 'react';
import { bool, shape } from 'prop-types';

import classes from './viewCard.scss';

const mockInfo = {
	profile:
		'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQr-A4wsT_5iHS2d34kPEZDJ5D_BchutzRLcg&usqp=CAU',
	fullName: 'Ryan Nguyen',
	bio: 'Frontend Developer at Tigren Solutions',
	email: 'contact@ryannguyen.info',
	phone: '+84971001421',
	pin: '124'
};

const ViewCard = (props = {}) => {
	const { isPreview, configs, links } = props;

	const info = mockInfo;

	const rootClass = isPreview ? classes.preview : classes.root;
	const themeClass = configs.theme;

	const socialLinks = useMemo(() => {
		const content = links.map((link) => {
			const { label, icon, value } = link;
			return (
				<li>
					{label} {value}
				</li>
			);
		});
		return <ul>{content}</ul>;
	}, [links]);

	return (
		<div className={rootClass}>
			<div className={themeClass}>
				<div className={themeClass.avatar}>
					<img src={info.profile} />
				</div>
				<p className={classes.name}>{info.fullName}</p>
				<p className={classes.bio}>{info.bio}</p>

				<div className={classes.socials}>{socialLinks}</div>
			</div>
		</div>
	);
};

ViewCard.propTypes = {
	info: shape,
	links: shape,
	configs: shape,
	isPreview: bool
};
ViewCard.defaultProps = {
	configs: {
		theme: 'simple'
	},
	isPreview: false
};
export default ViewCard;
