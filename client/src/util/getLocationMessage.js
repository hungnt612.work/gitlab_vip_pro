import BrowserPersistence from '@util/simplePersistence';

const storage = new BrowserPersistence();

const lang = storage.getItem('lang') || 'en';
import en from '@digicard/languages/en.json';
import vi from '@digicard/languages/vi.json';
import cz from '@digicard/languages/cz.json';

let translateMessages;
switch (lang) {
    case 'en':
        translateMessages = en;
        break;
    case 'vi':
        translateMessages = vi;
        break;
    case 'cz':
        translateMessages = cz;
        break;
    default:
        translateMessages = en;
}

export default translateMessages;
