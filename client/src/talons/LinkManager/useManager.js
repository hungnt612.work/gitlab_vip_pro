import { useCallback, useState } from 'react';

export const useManager = (props = {}) => {
	const [links, setLinks] = useState([]);

	const onCallbackLink = useCallback((links) => {
		setLinks(links);
	}, []);

	return {
		links,
		onCallbackLink
	};
};
