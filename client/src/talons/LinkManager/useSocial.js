import React, { useEffect } from 'react';
import { message } from 'antd';
import { useCallback, useState } from 'react';
import { useIntl } from 'react-intl';

import supportSocialTypes from './socialItems.json';

export const useSocial = (props = {}) => {
	const { onCallback } = props;

	const [items, setItems] = useState([]);
	const [isShowCreateModal, setIsShowCreateModal] = useState(false);

	const { formatMessage } = useIntl();

	useEffect(() => {
		onCallback(items);
	}, [items]);

	const toggleCreateModal = useCallback(
		(value) => {
			setIsShowCreateModal(value && !isShowCreateModal);
		},
		[setIsShowCreateModal, isShowCreateModal]
	);

	const handleAddLink = useCallback(
		(value) => {
			const addedToList = items.some((item) => item.type === value);
			if (addedToList) {
				setIsShowCreateModal(false);
				return message.warn(
					formatMessage({
						id: 'useSocial.addedToList',
						defaultMessage: 'This link added to list.'
					})
				);
			}

			const socialConfig = supportSocialTypes.find(
				(social) => social.type === value
			);

			if (socialConfig) {
				const newValues = [...items, socialConfig];
				setItems(newValues);
				setIsShowCreateModal(false);
			}
		},
		[
			items,
			setItems,
			formatMessage,
			setIsShowCreateModal,
			supportSocialTypes
		]
	);

	const onDragOver = (event) => {
		setItems(event.newState);
	};

	const onNavigate = (event) => {
		setItems(event.newState);
	};

	const onEditItem = useCallback(
		(type, fields) => {
			setItems((prevItems) => {
				return prevItems.map((item) => {
					if (item.type === type) {
						Object.entries(fields).forEach((field) => {
							const [key, newValue] = field;
							item[key] = newValue;
						});
					}
					return item;
				});
			});
		},
		[items, setItems]
	);

	const onDeleteItem = useCallback(
		(type) => {
			setItems((prevItems) => {
				return prevItems.filter((item) => item.type !== type);
			});
		},
		[items, setItems]
	);

	return {
		items,
		setItems,
		isShowCreateModal,
		toggleCreateModal,
		supportSocialTypes,
		handleAddLink,
		onDragOver,
		onNavigate,
		onDeleteItem,
		onEditItem
	};
};
