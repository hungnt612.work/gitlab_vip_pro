import { useCallback } from 'react';
import { useMutation } from '@apollo/client';

import operations from './signInPage.gql';

import BrowserPersistence from '@util/simplePersistence';
import { useHistory } from 'react-router';

const storage = new BrowserPersistence();

export const useSignInPage = (props = {}) => {
	const [signIn, { data, loading, error }] = useMutation(
		operations.signInMutation
	);

	const history = useHistory();

	const handleLogin = useCallback(
		async (values) => {
			const { email, password } = values;
			try {
				const userData = await signIn({
					variables: {
						email,
						password
					}
				});

				const { token, refreshToken } = userData?.data?.signIn;
				storage.setItem('token', token);
				storage.setItem('refresh_token', refreshToken);

				history.push('/welcome');
			} catch (err) {
				console.log(err);
			}
		},
		[history]
	);

	return {
		handleLogin,
		isLoading: loading
	};
};
