import { gql } from '@apollo/client';

const SIGN_IN_MUTATION = gql`
    mutation signIn($email: String!, $password: String!) {
        signIn(inputs: { email: $email, password: $password }) {
            userId
            token
            refreshToken
            tokenExpiration
        }
    }
`;

export default {
    signInMutation: SIGN_IN_MUTATION
};
